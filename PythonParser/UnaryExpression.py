#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('PythonParser.UnaryExpression')
def module():
    class NegativeExpression(UnaryExpression):
        __slots__    = (())
        display_name = '-'
        frill        = conjure_action_word('-', '-')


        scout_variables = scout_variables__a


    class NotExpression(UnaryExpression):
        __slots__    = (())
        display_name = 'not'
        frill        = NOT__W


        mutate          = produce_mutate__frill__a__priority('not-expression', PRIORITY_UNARY)
        scout_variables = scout_variables__a


    class StarArgument(UnaryExpression):
        __slots__    = (())
        display_name = '*-argument'
        frill        = conjure_star_sign('*')

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>


        scout_variables = scout_variables__a


    class StarParameter(UnaryExpression):
        __slots__    = (())
        display_name = '*-parameter'
        frill        = conjure_star_sign('*')

        #<atom>
        is_CRYSTAL_atom = true
        #</atom>

        is_PYTHON__identifier__or__star_parameter = true


        add_parameters       = add_parameters__a
        scout_default_values = scout_default_values__a


    conjure_negative_expression = produce_conjure_unary_expression('negative',    NegativeExpression)
    conjure_not_expression      = produce_conjure_unary_expression('not',         NotExpression)
    conjure_star_argument       = produce_conjure_unary_expression('*-argument',  StarArgument)
    conjure_star_parameter      = produce_conjure_unary_expression('*-parameter', StarParameter)


    NotExpression           .conjure_with_frill = static_method(conjure_not_expression)
    StarArgument            .conjure_with_frill = static_method(conjure_star_argument)
    StarParameter           .conjure_with_frill = static_method(conjure_star_parameter)
    TwosComplementExpression.conjure_with_frill = static_method(conjure_twos_complement_expression)


    #
    #   .mutate
    #
    NegativeExpression.mutate = produce_mutate__frill__a_with_priority(
                                    'negative_expression',
                                    PRIORITY_UNARY,
                                    conjure_negative_expression,
                                )

    StarArgument.mutate = produce_mutate__frill__a_with_priority(
                              'star_argument',
                              PRIORITY_TERNARY,
                              conjure_star_argument,
                          )

    TwosComplementExpression.mutate = produce_mutate__frill__a_with_priority(
                                          'twos_complement_expression',
                                          PRIORITY_TERNARY,
                                          conjure_twos_complement_expression,
                                      )


    #
    #   .transform
    #
    StarParameter.transform = produce_transform__frill_a('star_paramater', conjure_star_parameter)


    share(
        'conjure_negative_expression',  conjure_negative_expression,
        'conjure_not_expression',       conjure_not_expression,
        'conjure_star_argument',        conjure_star_argument,
        'conjure_star_parameter',       conjure_star_parameter,
    )
