#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.TripleTwig')
def module():
    require_module('CoreParser.Cache')
    require_module('CoreParser.ClassOrder')
    require_module('CoreParser.Method')
    require_module('CoreParser.ParserTrunk')


    @export
    class TripleTwig(ParserTrunk):
        __slots__ = ((
            'a',                        #   Any
            'b',                        #   Any
            'c',                        #   Any
        ))


        __init__       = construct__123
        __repr__       = portray__123
        count_newlines = count_newlines__123
        display_token  = display_token__123
        dump_token     = dump_token__123
        order          = order__abc
        write          = write__123


    TripleTwig.k1 = TripleTwig.a
    TripleTwig.k2 = TripleTwig.b
    TripleTwig.k3 = TripleTwig.c


    @export
    def produce_conjure_triple_twig(name, Meta):
        cache = create_cache(name, conjure_nub)

        return produce_conjure_unique_triple__312(name, Meta, cache)
