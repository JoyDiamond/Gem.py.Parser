#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.UnaryOrPostfixExpression')
def module():
    @share
    class UnaryOrPostfixExpression(ParserTrunk):
        __slots__ = ((
            'a',                        #   Expression
        ))


        __init__       = construct__a
        __repr__       = portray__a
        count_newlines = count_newlines__a
        display_token  = display_token__a
        order          = order__frill_a


    UnaryOrPostfixExpression.k1 = UnaryOrPostfixExpression.a


    @share
    def produce_conjure_unary_or_postfix_expression(name, Meta, postfix):
        cache   = create_cache(name, conjure_nub)
        lookup  = cache.lookup
        provide = cache.provide
        store   = cache.store


        def conjure_UnaryOrPostfixExpression_WithFrill(a, frill):
            UnaryOrPostfixExpression_WithFrill = lookup_adjusted_meta(Meta)

            if UnaryOrPostfixExpression_WithFrill is none:
                class UnaryOrPostfixExpression_WithFrill(Meta):
                    __slots__ = ((
                        'frill',                #   Operator*
                    ))


                    __init__       = construct__a__frill
                    __repr__       = portray__a__frill
                    count_newlines = count_newlines__a__frill
                    display_token  = (attribute(Meta, 'display_token__frill', none)) or (display_token__a__frill)


                UnaryOrPostfixExpression_WithFrill.k2 = UnaryOrPostfixExpression_WithFrill.frill


                if python_debug_mode:
                    UnaryOrPostfixExpression_WithFrill.__name__ = intern_arrange('%s_WithFrill', Meta.__name__)

                store_adjusted_meta(Meta, UnaryOrPostfixExpression_WithFrill)

            return UnaryOrPostfixExpression_WithFrill(a, frill)



        conjure_dual__21 = produce_conjure_unique_dual__21(
                name,
                conjure_UnaryOrPostfixExpression_WithFrill,
                cache,
                lookup,
                store,
            )


        meta_frill = Meta.frill


        #
        #   The order of the arguments is opposite for unary & postfix.
        #
        #       `postfix`:  `a`, `frill`
        #       `unary`:    `frill`, `a`
        #
        if postfix:
            @rename('conjure_%s', name)
            def conjure_postfix_expression(a, frill):
                if frill is meta_frill:
                    return (lookup(a)) or (provide(a, Meta(a)))

                return conjure_dual__21(a, frill)


            return conjure_postfix_expression


        @rename('conjure_%s', name)
        def conjure_unary_expression(frill, a):
            if frill is meta_frill:
                return (lookup(a)) or (provide(a, Meta(a)))

            return conjure_dual__21(a, frill)


        return conjure_unary_expression
