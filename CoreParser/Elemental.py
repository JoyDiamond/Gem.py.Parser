#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.Elemental')
def module():
    require_module('CoreParser.ActionWord')
    require_module('CoreParser.Atom')
    require_module('CoreParser.ClassOrder')


    @export
    class KeywordAndOperatorBase(ParserToken):
        __slots__ = (())

        class_order = CLASS_ORDER__NORMAL_TOKEN

        #<atom>
        if CRYSTAL_parser:
            is_CRYSTAL_atom                                  = false
            is_CRYSTAL_simple_atom__or__colon                = false
            is_CRYSTAL_simple_atom__or__right_brace          = false
            is_CRYSTAL_simple_atom__or__right_parenthesis    = false
            is_CRYSTAL_simple_atom__or__right_square_bracket = false

        if TREMOLITE_parser:
            is_TREMOLITE___simple_atom___or___set__right_brace   = false
            is_TREMOLITE___simple_atom___or___tilde__right_brace = false
        #</atom>

        if CRYSTAL_parser:
            is_and_sign   = false
            is_or_sign    = false
            is_plus_sign  = false
            is_tilde_sign = false

        if PYTHON_parser:
            is_all_index                             = false
            is_arguments_0                           = false
            is__arguments_0__or__left_parenthesis    = false
            is_colon__line_marker                    = false
            is_colon__right_square_bracket           = false
            is__comma__or__right_parenthesis         = false
            is_comma__right_parenthesis              = false
            is_comma__right_square_bracket           = false
            is_compare_operator                      = false
            is_dot                                   = false
            is_end_of_boolean_and_expression         = false
            is_end_of_boolean_or_expression          = false
            is_end_of_compare_expression             = false
            is_end_of_comprehension_expression       = false
            is_end_of_comprehension_expression_list  = false
            is_end_of_logical_and_expression         = false
            is_end_of_logical_or_expression          = false
            is_end_of_multiply_expression            = false
            is_end_of_normal_expression              = false
            is_end_of_normal_expression_list         = false
            is_end_of_PYTHON_arithmetic_expression   = false
            is_end_of_PYTHON_unary_expression        = false
            is_end_of_ternary_expression             = false
            is_end_of_ternary_expression_list        = false
            is_equal_sign                            = false
            is_keyword_and                           = false
            is_keyword_as                            = false
            is_keyword_else                          = false
            is_keyword_for                           = false
            is_keyword_if                            = false
            is_keyword_in                            = false
            is_keyword_not                           = false
            is_keyword_or                            = false
            is_keyword_return                        = false
            is_minus_sign                            = false
            is_modify_operator                       = false
            is_multiply_operator                     = false
            is__optional_comma__right_brace          = false
            is__optional_comma__right_parenthesis    = false
            is__optional_comma__right_square_bracket = false
            is_parameters_0                          = false
            is_power_operator                        = false
            is_PYTHON_arithmetic_operator            = false
            is_PYTHON_postfix_operator               = false
            is_star_sign                             = false
            is_tail_index                            = false


        if TREMOLITE_parser:
            is_end_of_TREMOLITE_and_expression        = false
            is_end_of_TREMOLITE_arithmetic_expression = false
            is_end_of_TREMOLITE_or_expression         = false
            is_end_of_TREMOLITE_range_expression      = false
            is_end_of_TREMOLITE_unary_expression      = false
            is_TREMOLITE__left_brace__set             = false
            is_TREMOLITE__left_brace__tilde           = false
            is_TREMOLITE_postfix_operator             = false
            is_TREMOLITE_range_operator               = false
            is_TREMOLITE_unary_operator               = false


        def __repr__(t):
            return arrange('<%s %s>', t.__class__.__name__, portray_string(t.s))


        def display_token(t):
            return arrange('{%s}', t.s)



    if JAVA_parser or PYTHON_parser:
        @export
        class KeywordImport(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = 'import'


        if JAVA_parser:
            [
                    conjure_keyword_import, conjure_keyword_import__ends_in_newline,
            ] = produce_conjure_action_word__ends_in_newline('keyword_import', KeywordImport)
        else:
            conjure_keyword_import = produce_conjure_action_word__normal('keyword_import', KeywordImport)


        export(
            'conjure_keyword_import',   conjure_keyword_import,
        )

        if JAVA_parser:
            export(
                'conjure_keyword_import__ends_in_newline',  conjure_keyword_import__ends_in_newline,
            )


    if TREMOLITE_parser:
        #
        #   Tremolite Parser
        #
        @export
        class KeywordLanguage(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = 'language'


        conjure_keyword_language = produce_conjure_action_word__normal('keyword_language', KeywordLanguage)

        export(
            'conjure_keyword_language',     conjure_keyword_language,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorAndSign(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '&'

            is_and_sign = true

            if PYTHON_parser:
                is_end_of_multiply_expression          = true
                is_end_of_PYTHON_arithmetic_expression = true
                is_end_of_PYTHON_unary_expression      = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorAtSign(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '@'

            is_at_sign = true


        conjure_at_sign = produce_conjure_action_word__normal('at_sign', OperatorAtSign)

        export(
            'conjure_at_sign',  conjure_at_sign,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorColon(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = ':'

            is_CRYSTAL_simple_atom__or__colon = true

            if CRYSTAL_parser:
                is_colon = true

            if PYTHON_parser:
                is_end_of_boolean_and_expression        = true
                is_end_of_boolean_or_expression         = true
                is_end_of_compare_expression            = true
                is_end_of_comprehension_expression_list = true
                is_end_of_comprehension_expression      = true
                is_end_of_logical_and_expression        = true
                is_end_of_logical_or_expression         = true
                is_end_of_multiply_expression           = true
                is_end_of_normal_expression_list        = true
                is_end_of_normal_expression             = true
                is_end_of_PYTHON_arithmetic_expression  = true
                is_end_of_PYTHON_unary_expression       = true
                is_end_of_ternary_expression_list       = true
                is_end_of_ternary_expression            = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_or_expression         = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


        [
                conjure_colon, conjure_colon__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('colon', OperatorColon)

        export(
            'conjure_colon',                    conjure_colon,
            'conjure_colon__ends_in_newline',   conjure_colon__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorComma(KeywordAndOperatorBase):
            __slots__      = (())
            display_name   = ','
            frill_estimate = 1

            if CRYSTAL_parser:
                is_comma = true

            if PYTHON_parser:
                is__comma__or__right_parenthesis       = true
                is_end_of_boolean_and_expression       = true
                is_end_of_boolean_or_expression        = true
                is_end_of_compare_expression           = true
                is_end_of_comprehension_expression     = true
                is_end_of_logical_and_expression       = true
                is_end_of_logical_or_expression        = true
                is_end_of_multiply_expression          = true
                is_end_of_normal_expression            = true
                is_end_of_PYTHON_arithmetic_expression = true
                is_end_of_PYTHON_unary_expression      = true
                is_end_of_ternary_expression           = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_or_expression         = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


        [
                conjure_CRYSTAL_comma, conjure_CRYSTAL_comma__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('comma', OperatorComma)

        export(
            'conjure_CRYSTAL_comma',                    conjure_CRYSTAL_comma,
            'conjure_CRYSTAL_comma__ends_in_newline',   conjure_CRYSTAL_comma__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorGreaterThanSign(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '>'


            if PYTHON_parser:
                is_compare_operator                    = true
                is_end_of_logical_and_expression       = true
                is_end_of_logical_or_expression        = true
                is_end_of_multiply_expression          = true
                is_end_of_normal_expression_list       = true
                is_end_of_normal_expression            = true
                is_end_of_PYTHON_arithmetic_expression = true
                is_end_of_PYTHON_unary_expression      = true


            def __repr__(t):
                if '\n' in t.s:
                    return arrange('{%s}', portray_string(t.s))

                return arrange('{%s}', t.s)


            def display_token(t):
                if t.s == ' > ':
                    return '{>}'

                return arrange('{%s %s}', t.display_name, portray_string(t.s))


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorLeftBrace(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '{'                                          #   }

            is_left_brace = true


            display_token = display_token__with_angle_signs
            dump_token    = dump_token__with_angle_signs


        [
            conjure_left_brace, conjure_left_brace__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('left_brace', OperatorLeftBrace)

        export(
            'conjure_left_brace',                   conjure_left_brace,
            'conjure_left_brace__ends_in_newline',  conjure_left_brace__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorLeftParenthesis(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '('                                          #   )

            is_left_parenthesis = true

            if PYTHON_parser:
                is__arguments_0__or__left_parenthesis = true
                is_PYTHON_postfix_operator            = true


        [
            conjure_left_parenthesis, conjure_left_parenthesis__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('left_parenthesis', OperatorLeftParenthesis)

        export(
            'conjure_left_parenthesis',                     conjure_left_parenthesis,
            'conjure_left_parenthesis__ends_in_newline',    conjure_left_parenthesis__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorLeftSquareBracket(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '['                                          #   ]

            is_left_square_bracket = true

            if PYTHON_parser:
                is_PYTHON_postfix_operator = true


        [
            conjure_left_square_bracket, conjure_left_square_bracket__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('left_square_bracket', OperatorLeftSquareBracket)

        export(
            'conjure_left_square_bracket',                      conjure_left_square_bracket,
            'conjure_left_square_bracket__ends_in_newline',     conjure_left_square_bracket__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorOrSign(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '|'

            is_or_sign = true

            if PYTHON_parser:
                is_end_of_logical_and_expression       = true
                is_end_of_multiply_expression          = true
                is_end_of_PYTHON_arithmetic_expression = true
                is_end_of_PYTHON_unary_expression      = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorPlusSign(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '+'

            is_plus_sign = true

            if PYTHON_parser:
                is_end_of_multiply_expression     = true
                is_end_of_PYTHON_unary_expression = true
                is_PYTHON_arithmetic_operator     = true


            if TREMOLITE_parser:
                is_end_of_TREMOLITE_range_expression = true
                is_end_of_TREMOLITE_unary_expression = true


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorRightBrace(KeywordAndOperatorBase):
            __slots__ = (())

            #  {
            display_name = '}'

            if CRYSTAL_parser:
                is_CRYSTAL_simple_atom__or__right_brace = true
                is_right_brace                          = true

            if PYTHON_parser:
                is_end_of_boolean_and_expression        = true
                is_end_of_boolean_or_expression         = true
                is_end_of_compare_expression            = true
                is_end_of_comprehension_expression_list = true
                is_end_of_comprehension_expression      = true
                is_end_of_logical_and_expression        = true
                is_end_of_logical_or_expression         = true
                is_end_of_multiply_expression           = true
                is_end_of_normal_expression_list        = true
                is_end_of_normal_expression             = true
                is_end_of_PYTHON_arithmetic_expression  = true
                is_end_of_PYTHON_unary_expression       = true
                is_end_of_ternary_expression_list       = true
                is_end_of_ternary_expression            = true
                is__optional_comma__right_brace         = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_or_expression         = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


            display_token = display_token__with_angle_signs
            dump_token    = dump_token__with_angle_signs


        [
            conjure_right_brace, conjure_right_brace__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('right_brace', OperatorRightBrace)

        export(
            'conjure_right_brace',                      conjure_right_brace,
            'conjure_right_brace__ends_in_newline',     conjure_right_brace__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorRightParenthesis(KeywordAndOperatorBase):
            __slots__ = (())

            #  (
            display_name = ')'

            if CRYSTAL_parser:
                is_CRYSTAL_simple_atom__or__right_parenthesis = true
                is_right_parenthesis                          = true

            if PYTHON_parser:
                is__comma__or__right_parenthesis        = true
                is_end_of_boolean_and_expression        = true
                is_end_of_boolean_or_expression         = true
                is_end_of_compare_expression            = true
                is_end_of_comprehension_expression_list = true
                is_end_of_comprehension_expression      = true
                is_end_of_logical_and_expression        = true
                is_end_of_logical_or_expression         = true
                is_end_of_multiply_expression           = true
                is_end_of_normal_expression_list        = true
                is_end_of_normal_expression             = true
                is_end_of_PYTHON_arithmetic_expression  = true
                is_end_of_PYTHON_unary_expression       = true
                is_end_of_ternary_expression_list       = true
                is_end_of_ternary_expression            = true
                is__optional_comma__right_parenthesis   = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_or_expression         = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


        [
            conjure_right_parenthesis, conjure_right_parenthesis__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('right_parenthesis', OperatorRightParenthesis)

        export(
            'conjure_right_parenthesis',                    conjure_right_parenthesis,
            'conjure_right_parenthesis__ends_in_newline',   conjure_right_parenthesis__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorRightSquareBracket(KeywordAndOperatorBase):
            __slots__ = (())

            #   [
            display_name = ']'

            if CRYSTAL_parser:
                is_CRYSTAL_simple_atom__or__right_square_bracket = true
                is_right_square_bracket                          = true

            if PYTHON_parser:
                is_end_of_boolean_and_expression         = true
                is_end_of_boolean_or_expression          = true
                is_end_of_compare_expression             = true
                is_end_of_comprehension_expression_list  = true
                is_end_of_comprehension_expression       = true
                is_end_of_logical_and_expression         = true
                is_end_of_logical_or_expression          = true
                is_end_of_multiply_expression            = true
                is_end_of_normal_expression_list         = true
                is_end_of_normal_expression              = true
                is_end_of_PYTHON_arithmetic_expression   = true
                is_end_of_PYTHON_unary_expression        = true
                is_end_of_ternary_expression_list        = true
                is_end_of_ternary_expression             = true
                is__optional_comma__right_square_bracket = true

            if TREMOLITE_parser:
                is_end_of_TREMOLITE_and_expression        = true
                is_end_of_TREMOLITE_arithmetic_expression = true
                is_end_of_TREMOLITE_or_expression         = true
                is_end_of_TREMOLITE_range_expression      = true
                is_end_of_TREMOLITE_unary_expression      = true


        [
            conjure_right_square_bracket, conjure_right_square_bracket__ends_in_newline,
        ] = produce_conjure_action_word__ends_in_newline('right_square_bracket', OperatorRightSquareBracket)

        export(
            'conjure_right_square_bracket',                     conjure_right_square_bracket,
            'conjure_right_square_bracket__ends_in_newline',    conjure_right_square_bracket__ends_in_newline,
        )


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorSolidus(KeywordAndOperatorBase):
            __slots__    = (())
            display_name = '/'

            if PYTHON_parser:
                is_end_of_PYTHON_unary_expression = true
                is_multiply_operator              = true

            if TREMOLITE_parser:
                is_TREMOLITE_postfix_operator = true
                is_TREMOLITE_unary_operator   = true


    if PYTHON_parser or TREMOLITE_parser:
        @export
        class OperatorTildeSign(KeywordAndOperatorBase):
            __slots__     = (())
            display_name  = '~'

            is_tilde_sign = true

            if TREMOLITE_parser:
                is_TREMOLITE_unary_operator = true


    if PYTHON_parser or TREMOLITE_parser:
        initialize_action_word__Meta(
            ((
                ((  '&',    OperatorAndSign             )),
                ((  '(',    OperatorLeftParenthesis     )),
                ((  ')',    OperatorRightParenthesis    )),
                ((  '+',    OperatorPlusSign            )),
                ((  '/',    OperatorSolidus             )),
                ((  ':',    OperatorColon               )),
                ((  '>',    OperatorGreaterThanSign     )),
                ((  '@',    OperatorAtSign              )),
                ((  '[',    OperatorLeftSquareBracket   )),
                ((  ']',    OperatorRightSquareBracket  )),
                ((  '{',    OperatorLeftBrace           )),
                ((  '|',    OperatorOrSign              )),
                ((  '}',    OperatorRightBrace          )),
                ((  '~',    OperatorTildeSign           )),
            )),
        )

    if PYTHON_parser or TREMOLITE_parser:
        AT_SIGN                 = conjure_at_sign             ('@')
        LEFT_BRACE              = conjure_left_brace          ('{')
        LEFT_PARENTHESIS        = conjure_left_parenthesis    ('(')
        LEFT_SQUARE_BRACKET     = conjure_left_square_bracket ('[')

        #{
        RIGHT_BRACE             = conjure_right_brace         ('}')

        #(
        RIGHT_PARENTHESIS       = conjure_right_parenthesis   (')')

        #[
        RIGHT_SQUARE_BRACKET    = conjure_right_square_bracket(']')

        TILDE_SIGN              = conjure_action_word         ('~', '~')
        W__AND_SIGN__W          = conjure_action_word         ('&', ' & ')
        W__COLON__W             = conjure_colon               (' : ')
        W__GREATER_THAN_SIGN__W = conjure_action_word         ('>', ' > ')
        W__OR_SIGN__W           = conjure_action_word         ('|', ' | ')

        export(
            'AT_SIGN',                  AT_SIGN,
            'LEFT_BRACE',               LEFT_BRACE,
            'LEFT_PARENTHESIS',         LEFT_PARENTHESIS,
            'LEFT_SQUARE_BRACKET',      LEFT_SQUARE_BRACKET,
            'RIGHT_BRACE',              RIGHT_BRACE,
            'RIGHT_PARENTHESIS',        RIGHT_PARENTHESIS,
            'RIGHT_SQUARE_BRACKET',     RIGHT_SQUARE_BRACKET,
            'TILDE_SIGN',               TILDE_SIGN,
            'W__AND_SIGN__W',           W__AND_SIGN__W,
            'W__COLON__W',              W__COLON__W,
            'W__GREATER_THAN_SIGN__W',  W__GREATER_THAN_SIGN__W,
            'W__OR_SIGN__W',            W__OR_SIGN__W,
        )

    if PYTHON_parser:
        COLON         = conjure_colon        (':')
        COMMA__W      = conjure_CRYSTAL_comma(', ')
        W__SOLIDUS__W = conjure_action_word  ('/', ' / ')

        export(
            'COLON',            COLON,
            'COMMA__W',         COMMA__W,
            'W__SOLIDUS__W',    W__SOLIDUS__W,
        )

    if TREMOLITE_parser:
        COLON__W               = conjure_colon              (': ')
        LEFT_BRACE__W          = conjure_left_brace         ('{ ')
        LEFT_SQUARE_BRACKET__W = conjure_left_square_bracket('[ ')
        SOLIDUS                = conjure_action_word        ('/', '/')

        #{
        W__RIGHT_BRACE          = conjure_right_brace(' }')

        #[
        W__RIGHT_SQUARE_BRACKET = conjure_right_square_bracket(' ]')


        export(
            'COLON__W',                 COLON__W,
            'LEFT_BRACE__W',            LEFT_BRACE__W,
            'LEFT_SQUARE_BRACKET__W',   LEFT_SQUARE_BRACKET__W,
            'SOLIDUS',                  SOLIDUS,
            'W__RIGHT_BRACE',           W__RIGHT_BRACE,
            'W__RIGHT_SQUARE_BRACKET',  W__RIGHT_SQUARE_BRACKET,
        )


    #
    #   is_CRYSTAL_close_or_open_operator
    #
    if CRYSTAL_parser:
        if PYTHON_parser:
            is_CRYSTAL_close_or_open_operator = {
                    #   {[(
                    ')' : 7,
                    ']' : 7,
                    '}' : 7,
                }.get

        if TREMOLITE_parser:
            is_CRYSTAL_close_or_open_operator = {
                    '('  : 3,
                    ')'  : 7,
                    '['  : 3,
                    ']'  : 7,
                    '{|' : 3,
                    '|}' : 7,
                }.get

        export(
            'is_CRYSTAL_close_or_open_operator',    is_CRYSTAL_close_or_open_operator,
        )
