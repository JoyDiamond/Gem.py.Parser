#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.UnaryExpression')
def module():
    require_module('CoreParser.UnaryOrPostfixExpression')


    @export
    class UnaryExpression(UnaryOrPostfixExpression):
        __slots__ = (())


        class_order = CLASS_ORDER__UNARY_EXPRESSION
        dump_token  = dump_token__frill__a


        def write(t, w):
            w(t.frill.s)
            t.a.write(w)


    @export
    def produce_conjure_unary_expression(name, Meta):
        return produce_conjure_unary_or_postfix_expression(name, Meta, false)


    @export
    class TwosComplementExpression(UnaryExpression):
        __slots__    = (())
        display_name = '~'
        frill        = TILDE_SIGN


        if PYTHON_parser:
            scout_variables = scout_variables__a


    conjure_twos_complement_expression = produce_conjure_unary_expression('twos-complement', TwosComplementExpression)


    export(
        'conjure_twos_complement_expression',   conjure_twos_complement_expression,
    )
