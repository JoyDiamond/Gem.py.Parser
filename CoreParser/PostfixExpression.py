#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.PostfixExpression')
def module():
    require_module('CoreParser.UnaryOrPostfixExpression')


    @export
    class PostfixExpression(UnaryOrPostfixExpression):
        __slots__ = (())


        class_order = CLASS_ORDER__POSTFIX_EXPRESSION
        dump_token  = dump_token__a__frill


        def write(t, w):
            t.a.write(w)
            w(t.frill.s)


    @export
    def produce_conjure_postfix_expression(name, Meta):
        return produce_conjure_unary_or_postfix_expression(name, Meta, true)
