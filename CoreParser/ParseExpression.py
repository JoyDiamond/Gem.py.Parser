#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@module('CoreParser.ParseExpression')
def module():
    @export
    def produce_parse_LANGUAGE__X_expression__left_operator(
            language,
            name,

            conjure_LANGUAGE_X_expression_many,
            conjure_LANGUAGE_X_expression,                              #   May be 0
            name__is_end_of_X_expression,
            name__is_X_expression,
            parse_LANGUAGE__W_expression,
            tokenize_LANGUAGE_operator,
    ):
        assert type(language)                           is String
        assert type(name)                               is String

        assert type(conjure_LANGUAGE_X_expression_many) is Function

        if conjure_LANGUAGE_X_expression is not 0:
            assert type(conjure_LANGUAGE_X_expression)  is Function

        assert type(name__is_end_of_X_expression)       is String
        assert type(name__is_X_expression)              is String
        assert type(parse_LANGUAGE__W_expression)       is Function
        assert type(tokenize_LANGUAGE_operator)         is Function


        if conjure_LANGUAGE_X_expression is 0:
            @rename('parse_%s__%s__left_operator', language, name)
            def parse_LANGUAGE__X_expression__left_operator__use_expression_meta(left, left_operator):
                assert attribute(left_operator, name__is_X_expression)

                right = parse_LANGUAGE__W_expression()

                operator = qk()

                if operator is 0:
                    if qn() is not 0:
                        return left_operator.expression_meta(left, left_operator, right)

                    operator = tokenize_LANGUAGE_operator()

                    if attribute(operator, name__is_end_of_X_expression):
                        wk(operator)

                        return left_operator.expression_meta(left, left_operator, right)
                else:
                    if attribute(operator, name__is_end_of_X_expression):
                        return left_operator.expression_meta(left, left_operator, right)

                    wk0()

                if not attribute(operator, name__is_X_expression):
                    raise_unknown_line()

                many       = [left, right]
                many_frill = [left_operator, operator]

                while 7 is 7:
                    many.append(parse_LANGUAGE__W_expression())

                    operator = qk()

                    if operator is 0:
                        if qn() is not 0:
                            break

                        operator = tokenize_LANGUAGE_operator()

                        if attribute(operator, name__is_end_of_X_expression):
                            wk(operator)
                            break
                    else:
                        if attribute(operator, name__is_end_of_X_expression):
                            break

                        wk0()

                    if not attribute(operator, name__is_X_expression):
                        raise_unknown_line()

                    many_frill.append(operator)

                return conjure_LANGUAGE_X_expression_many(many, many_frill)
        else:
            @rename('parse_%s__%s__left_operator', language, name)
            def parse_LANGUAGE__X_expression__left_operator__use_expression_meta(left, left_operator):
                assert attribute(left_operator, name__is_X_expression)

                right = parse_LANGUAGE__W_expression()

                operator = qk()

                if operator is 0:
                    if qn() is not 0:
                        return conjure_LANGUAGE_X_expression(left, left_operator, right)

                    operator = tokenize_LANGUAGE_operator()

                    if attribute(operator, name__is_end_of_X_expression):
                        wk(operator)

                        return conjure_LANGUAGE_X_expression(left, left_operator, right)
                else:
                    if attribute(operator, name__is_end_of_X_expression):
                        return conjure_LANGUAGE_X_expression(left, left_operator, right)

                    wk0()

                if not attribute(operator, name__is_X_expression):
                    raise_unknown_line()

                many       = [left, right]
                many_frill = [left_operator, operator]

                while 7 is 7:
                    many.append(parse_LANGUAGE__W_expression())

                    operator = qk()

                    if operator is 0:
                        if qn() is not 0:
                            break

                        operator = tokenize_LANGUAGE_operator()

                        if attribute(operator, name__is_end_of_X_expression):
                            wk(operator)
                            break
                    else:
                        if attribute(operator, name__is_end_of_X_expression):
                            break

                        wk0()

                    if not attribute(operator, name__is_X_expression):
                        raise_unknown_line()

                    many_frill.append(operator)

                return conjure_LANGUAGE_X_expression_many(many, many_frill)


        return parse_LANGUAGE__X_expression__left_operator__use_expression_meta
