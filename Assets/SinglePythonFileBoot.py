#
#   Copyright (c) 2017-2018 Joy Diamond.  All rights reserved.
#
@boot('Boot')
def boot():
    #
    #   This really belongs in Capital.Core, but is here since we need it during Boot
    #
    Python_System = __import__('sys')
    Python_Path   = __import__('os.path').path


    is_python_2 = (Python_System.version_info.major is 2)
#   is_python_3 = (Python_System.version_info.major is 3)


    Python_BuiltIn = __import__('__builtin__'  if is_python_2 else   'builtins')


    #
    #   Python Keywords
    #
#   false = False
#   none  = None
#   true  = True


    #
    #   Python Functions
    #
    intern_string = (Python_BuiltIn   if is_python_2 else   Python_System).intern
    module_path   = Python_System.path
    path_absolute = Python_Path.abspath
    path_join     = Python_Path.join
    type          = Python_BuiltIn.type


    #
    #   Python Types
    #
    ModuleType = type(Python_System)


    #
    #   python_modules (also lookup_python_module & store_python_module)
    #
    python_modules       = Python_System.modules
    store_python_module  = python_modules.__setitem__


    #
    #   Capital_name
    #
    Capital_name = intern_string('Capital')


    #
    #   Calculate & store root path
    #
    root_path = intern_string(path_absolute(path_join(module_path[0], '../..')))

    module_path[0] = root_path


    #
    #   Create initial modules
    #
    def create_module(module_name):
        module_name = intern_string(module_name)
        module_path = intern_string(path_join(root_path, module_name))

        module = ModuleType(module_name)

        module.__file__ = intern_string(path_join(module_path, '__init__.py'))
        module.__path__ = [module_path]

        assert module_name not in python_modules

        store_python_module(module_name, module)

        if (module_name.rfind('.') == -1) and (module_name != 'Capital'):
            produce_export_transport_and_share = Capital.Shared.produce_export_transport_and_share
            store_capital_module               = Capital.Shared.store_capital_module

            produce_export_transport_and_share(module)
            store_capital_module(module_name, module)

        return module


    #
    #   Set flag to indicate using fast loading mode
    #
    Capital = create_module('Capital')

    Capital.fast_cache = fast_cache = {}

    store_fast = fast_cache.__setitem__


    def module(module_name):
        def execute(f):
            store_fast(module_name, f)

            return module


        return execute


    __import__(__name__).module = module


    def boot():
        fast_cache.pop('Capital.Boot')()

        f           = fast_cache.pop('PythonParser.Main')
        module_main = __import__('__main__').module('PythonParser.Main')

        module_main(f)


    return boot
